const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({
	userId: {
		type: String,
		required: true,
	},
	userName: {
		type: String
	},
	products : [{
		productId: {
			type: String
		},
		productName: {
			type: String
		},
		quantity: {
			type: Number,
			default: 1
		},
		price: {
			type: Number
		}
	}],
	totalAmount: {
		type: Number,

	},
	address: {
		type: String,
		required: [true, "Address is required"]
	},
	status: {
		type: String,
		default: "pending"
	}

},{
	timestamps: true
});


module.exports = mongoose.model("Order", orderSchema);