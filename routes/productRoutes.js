const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productController = require("../controllers/productController.js");

// ROUTES


// Routes for creating a product
router.post("/addProduct", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.addProduct(req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You're not an ADMIN")
	}
});



// Routes for retrieving product
router.get("/", (req, res) => {
	productController.getActiveProducts().then((resultFromController) => res.send(resultFromController))
});


// Routes for retieving a single product
router.get("/find/:productId", (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send (resultFromController))
});

// Routes for update a single product
router.put("/update/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.updateProduct(req.params,req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You're not an ADMIN")
	}
});

// Routes for archiving a single product
router.patch("/archive/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.archiveProduct(req.params).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You're not an ADMIN")
	}
});


// Routes for activating a single product
router.patch("/activate/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.activateProduct(req.params).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You're not an ADMIN")
	}
});


// Routes for getting all product ADMIN ONLY
router.get("/allProducts", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.allProduct().then((resultFromController) => res.send(resultFromController))
	}else{
		res.status(403).send("You're not an ADMIN")
	}
	
});


















module.exports = router;