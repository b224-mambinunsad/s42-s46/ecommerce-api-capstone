const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const cartController = require("../controllers/cartController.js");

// ROUTES

// Routes for adding products on the cart authenticated user only
router.post("/addToCart/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == false){
		cartController.addToCart(req.params, userData, req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You are not allowed to do this")
	}
});


// Routes for updating authenticated user cart user only
router.patch("/update/:cartId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == false){
		cartController.updateCart(userData, req.params, req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You are not allowed to do this")
	}
});


// Routes for deleting authenticated user cart user only
router.delete("/delete/:cartId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == false){
		cartController.deleteCart(userData, req.params).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You are not allowed to do this")
	}
});


// Routes for retrieving user cart
router.get("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == false){
		cartController.getCart(userData).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You are not allowed to do this")
	}
});


// Routes for ordering the products in cart
router.post("/orderCart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == false){
		cartController.orderCart(userData, req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You are not allowed to do this")
	}
});









module.exports = router;