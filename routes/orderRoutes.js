const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const orderController = require("../controllers/orderController.js");

// ROUTES


// Routes for Non Admin Create Order
router.post("/addOrder/:productId", auth.verify, (req , res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == false){
		orderController.addOrder(req.params, req.body, userData).then((resultFromController) => res.send(resultFromController))
	}else{
		res.status(403).send("You cant order you are a ADMIN")
	}
});


// Routes for retrieving User order (User Only)
router.get("/myOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == false){
		orderController.getOrder(userData).then((resultFromController) => res.send (resultFromController))
	}else {
		res.status(403).send("You cant view orders you are a ADMIN")
	}
});


// Routes for updating User Order address or cancel if the status is pending (User Only)
router.patch("/myOrders/:orderId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == false){
		orderController.updateUserOrder(userData, req.params, req.body).then((resultFromController) => res.send (resultFromController))
	}else {
		res.status(403).send("You cant view tihs you are a ADMIN")
	}
});




// Routes for retrieving all Orders based on status ADMIN ONLY
router.get("/allOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		orderController.allOrders(req.body).then((resultFromController) => res.send (resultFromController))
	}else {
		res.status(403).send("You are not authorize to view this")
	}
});


// Routes for updating status of specific Orders ADMIN ONLY
router.patch("/updateOrder/:orderId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		orderController.updateOrder(req.params, req.body).then((resultFromController) => res.send (resultFromController))
	}else {
		res.status(403).send("You are not authorize to view this")
	}
});












module.exports = router;