const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const userController = require("../controllers/userController.js");

// ROUTES


// Routes for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then((resultFromController) => {
		res.send(resultFromController)
	})
});


// Routes for user login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then((resultFromController) => {
		res.send(resultFromController)
	})
});


// Routes for retrieving user details
router.get("/details", auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getDetails(userData).then((resultFromController) => res.send(resultFromController))
});


// Routes for setting user as ADMIN (ADMIN ONLY)
router.patch("/setAdmin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.email === "admin@email.com"){
		userController.setAdmin(req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You're not the owner")
	}
});

// Routes for getting all user details (Owner ONLY)
router.get("/getAllDetails", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.email === "admin@email.com"){
		userController.getAllDetails().then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You're not the owner")
	}
});
















module.exports = router;