const Product = require("../models/Product.js");



// CONTROLLER FUNCTIONS


// Function for adding product with validation for Existing Product
module.exports.addProduct = async (reqBody) => {
	try {
		const findProduct =  await Product.find({name: reqBody.name})
		if(findProduct.length === 0){
			let newProduct = new Product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price,
				stock: reqBody.stock
			});
			await newProduct.save()
			return `Successfully added a ${newProduct.name} Product` //true
		}else {
			return "Product name is already exists" // false
		}
	}catch(error){
		return "Error" //false
	}
};



// Function for retrieving products
module.exports.getActiveProducts = async () => {
	try {
		const activeProduct = await Product.find({isActive: true})
		if(activeProduct.length !== 0){
			return activeProduct
		}else {
			return false
		}
	}catch(error){
		return "Error" //false
	}

};


// Function for retrieving a single product
module.exports.getProduct = async (reqParams) => {
	try {
		const singleProduct =  await Product.findById(reqParams.productId)
		if(singleProduct.isActive == true){
			return singleProduct
		}else {
			return "This Product is out of stock" // false
		}
	}catch(error){
		return "Error"
	}
};


// Function for updating products
module.exports.updateProduct = async (reqParams, reqBody) => {
	try {
		let data = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			stock: reqBody.stock
		}
		const update = await Product.findByIdAndUpdate(reqParams.productId, data)
		if(update){
			return `Product ${update.name} successfully update` //true
		}else{
			return "Cannot find and update product" //false
		}
	}catch(error){
		return "Error" //false
	}

};


// Function for arching a product
module.exports.archiveProduct = async (reqParams) => {
	try {
		let data = {
			isActive: false
		}
		const update = await Product.findByIdAndUpdate(reqParams.productId, data)
		if(update){
			return `Product ${update.name} successfully archived` //true
		}else{
			return "Cannot find and archive product" //false
		}
	}catch(error){
		return "Error" //false
	}

};

// Function for activating a product
module.exports.activateProduct = async (reqParams) => {
	try {
		let data = {
			isActive: true
		}
		const update = await Product.findByIdAndUpdate(reqParams.productId, data)
		if(update){
			return `Product ${update.name} successfully activate` //true
		}else{
			return "Cannot find and activate product" //false
		}
	}catch(error){
		return "Error" //false
	}

};


// Function for retrieving all products ADMIN ONLY
module.exports.allProduct = async () => {
	try {
		const allProduct = await Product.find()
		if(allProduct){
			return allProduct
		}else {
			return false
		}
	}catch(error){
		return "Error" //false
	}

};