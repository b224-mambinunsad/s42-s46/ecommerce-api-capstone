const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



// CONTROLLER FUNCTIONS

// Function for user registration with validation if email is existing
module.exports.registerUser = async (reqBody) => {
	try {
		const findEmail =  await User.find({email: reqBody.email})

		if(findEmail == false){
			let newUser = new User({
				name: reqBody.name,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			});
			await newUser.save()
			return "Successfully Registered" //true
		}else {
			return "Email is already exists" // false
		}
	}catch(error){
		return "Error" //false
	}
};


// Function for users login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return "Wrong email" //false
		}else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else {
				return "Wrong password" // false
			}
		}
	})
};


// Function for retrieving user details
module.exports.getDetails = async (data) => {
	try {
		const details =  await User.findById(data.id)
		if(details){
			details.password = "**********"
			return details
		}else{
			return "You are not allowed to view this details" // false
		}
	}catch(error){
		return "Error" //false
	}
};

// Function for setting user as Admin (ADMIN ONLY)
module.exports.setAdmin = async (reqBody) => {
	try{
		const data = await User.findOne({email: reqBody.email})
		if(data == null){
			return "Cannot find email"
		}else{
			let update = {
				isAdmin: true
			}
			const makeAdmin = await User.findByIdAndUpdate(data.id, update)
			return `User ${data.name} has been successfully set as ADMIN`
		}
	}catch(error){
		return "Error"
	}
};

// Function for getting all user details (OWNER ONLY)
module.exports.getAllDetails = async () => {
	try {
		const allDetails = await User.find()
		if (allDetails.length === 0){
			return "No user on database" //false
		}else {
			return allDetails //true
		}
	}catch(error){
		return "Error" //false
	}
};