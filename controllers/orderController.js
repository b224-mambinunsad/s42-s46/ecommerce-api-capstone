const Order = require("../models/Order.js");
const Product = require("../models/Product.js");


// CONTROLLER FUNCTIONS



// Function for creating a order NOT ADMIN and updating Product Stock/isActive
module.exports.addOrder = async (reqParams, reqBody, data) => {
		try {
			const product = await Product.findById(reqParams.productId)
			if(product == null){
				return "Cannot find this product" //false
			}else if(product.stock - reqBody.quantity < 0){
				return "Out of stock! please order based on qty of this product stock" //false
			}else{
				let order = new Order ({
					userId: data.id,
					userName: data.name,
					products: [{
						productId: product.id,
						productName: product.name,
						quantity: reqBody.quantity || 1,
						price: product.price
					}],
					totalAmount: product.price * (reqBody.quantity || 1),
					address: reqBody.address
				});
				let update = {
					stock: product.stock - (reqBody.quantity || 1)
				}
				if(update.stock <= 0){
					 update = {
						stock: 0,
						isActive: false
					}
				}
				await Product.findByIdAndUpdate(reqParams.productId, update)
				await order.save()
				return `You successfully order ${product.name}` //true
			}	
	}catch(error){

		return "Error" //false
	}
};



// Function for retrieving user order (User Only)
module.exports.getOrder = async (data) => {
	try {
		const orders = await Order.find({userId: data.id})
		if(orders.length === 0){
			return "You dont have order" //false
		}else{
			return orders //true
		}
	}catch(error){
		return "Error" // false
	}
};


// Function for updating user order address or cancel order if it is not pending (USER ONLY)

module.exports.updateUserOrder = async (data, reqParams, reqBody) => {
	try {
		const orderStatus = await Order.findById(reqParams.orderId)
		if(orderStatus == null){
			return "Cannot find this product" //false
		}else if(data.id !== orderStatus.userId){
			return "You are not the user of this order" //false
		}else if(orderStatus.status === "pending"){
			let update = {
				address: reqBody.address
			}
			let cancel = {
				status: "cancelled"
			}
			if(reqBody.status === "cancelled"){
				const product = await Product.findById(orderStatus.products[0].productId)
				let stock = {
					stock: product.stock + orderStatus.products[0].quantity
				}
				if(product.isActive == false){
					stock = {
						stock: product.stock + orderStatus.products[0].quantity,
						isActive: true
					}
				}
				await Product.findByIdAndUpdate(product.id, stock)
				await Order.findByIdAndUpdate(orderStatus.id, cancel)
				return "Order has been successfully cancelled" //true
			}else if(reqBody.address){
				await Order.findByIdAndUpdate(orderStatus.id, update)
				return "Order has been successfully updated" //true
			}else{
				return "Unable to read the property you defined" //false
			}
		}else{
			return "You cannot update this order" //false
		}
	}catch(error){
		return "Error" // false
	}
};

// Function for getting all orders based on status (ADMIN ONLY)
module.exports.allOrders = async (reqBody) => {
	try {
		const orders = await Order.find({status: reqBody.status})
		if(orders.length === 0){
			return `Cannot find ${reqBody.status} products` //false
		}else{
			return orders //true
		}
	}catch(error){
		return "Error" // false
	}
};


// Function for updating Orders (ADMIN ONLY)
module.exports.updateOrder = async (reqParams, reqBody) => {
	try {
		let update = {
			status: reqBody.status
		}
		await Order.findByIdAndUpdate(reqParams.orderId, update)
		return "Order status Successfully updated" //true
	}catch(error){
		return "Error" //false
	}
};
