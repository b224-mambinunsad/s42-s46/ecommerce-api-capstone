const Cart = require("../models/Cart.js")
const Order = require("../models/Order.js");
const Product = require("../models/Product.js");


// CONTROLLER FUNCTIONS

// Function for adding product to user cart
module.exports.addToCart = async (reqParams, data, reqBody) => {
		try {
			const product = await Product.findById(reqParams.productId)
			
			if(product == null){
				return "Cannot find this product" //false
			}else if(product.stock - reqBody.quantity < 0){
				return "Out of stock! please order based on qty of this product stock" //false
			}else{
				const userCarts = await Cart.find({userId: data.id})
				let cart = new Cart ({
					userId: data.id,
					products: [{
						productId: product.id,
						productName: product.name,
						quantity: reqBody.quantity || 1,
						price: product.price
					}],
					totalAmount: product.price * (reqBody.quantity || 1)	
				});
				// This will check user cart if empty it will order will be save as new cart
				let cartProductNames = [];
				if(userCarts.length === 0){
					await cart.save()
					return "Successfully added to your cart" //true
				}else{
					for(i = 0; i < userCarts.length; i++){
						cartProductNames.push(userCarts[i].products)
					}
				};
				// This for loop check if the order product of user is arleady on his cart
				let isDuplicate;
				for(i = 0; i< cartProductNames.length; i++){
					if(product.name === cartProductNames[i][0].productName){
						isDuplicate = true
						break;
					}else{
						isDuplicate = false
					}
				};
				// This will add new cart if product is not already on cart of user
				if(isDuplicate === true){
					return "Duplicate product found on your cart" // false
				}else{
					await cart.save()
					return "Successfully added to your cart" // true
				}
			}
	}catch(error){

		return "Error" //false
	}
};


// Function for updating a users cart 
module.exports.updateCart = async (userData, reqParams, reqBody) => {
	try{
		const cart = await Cart.findById(reqParams.cartId)
		if(cart == null){
			return "Cannot find Cart Product" //false
		}else if(userData.id === cart.userId){
			let update = {
				products: [{
					productId: cart.products[0].productId,
					productName: cart.products[0].productName,
					quantity: reqBody.quantity,
					price: cart.products[0].price
				}],
				totalAmount: cart.products[0].price * reqBody.quantity

			};
			await Cart.findByIdAndUpdate(cart.id, update)
			return "Cart has been updated" // true
		}else {
			return "You are not the owner of this cart"// false
		}
		
	}catch(error){
		return "Error" //false
	}
};


// Function for deleting a user cart
module.exports.deleteCart = async (userData, reqParams) => {
	try {
		const cart = await Cart.findById(reqParams.cartId)
		if(cart == null){
			return "Cannot find Cart Product" //false
		}else if(userData.id === cart.userId){
			await Cart.findByIdAndDelete(reqParams.cartId)
			return "Successfully removed this product from your cart" //true
		}else{
			return "You are not the owner of this cart"
		}
	}catch(error){
		return "Error" //false
	}
};


// Function for retrieving user cart
module.exports.getCart = async (data) => {
	try {
		const cart = await Cart.find({userId: data.id})
		if(cart.length === 0){
			return "No product found on your cart" //false
		}else{
			return cart //true
		}
	}catch(error){
		return "Error" //false
	}
};


// Function for ording the cart of the user
module.exports.orderCart = async (data,reqBody) => {
	try{
		const userCarts = await Cart.find({userId: data.id})
		if(userCarts.length === 0){
			return "You dont have products on your cart" //false
		}else{
			let amount = 0;
			// this for loop will get the total amount of the ordered cart
			for(i = 0; i < userCarts.length; i++){
				 amount += userCarts[i].totalAmount
			};
			let order = new Order ({
				userId: data.id,
				userName: data.name,
				address: reqBody.address,
				products: [],
				totalAmount: amount,
				address: reqBody.address
			});
			// This for loop is for pushing the products order to the var of order
			// and update the Product stocks
			for(i = 0; i < userCarts.length; i++){
				// This will push the products orders into the var of order
				order.products.push({
					productId: userCarts[i].products[0].productId,
					productName: userCarts[i].products[0].productName,
					quantity: userCarts[i].products[0].quantity,
					price: userCarts[i].products[0].price,
				})

				// This is for updating the products stock 
				let x = await Product.findById(userCarts[i].products[0].productId)
				let y = userCarts[i].products[0].quantity
				if(x){
					let update = {
						stock: x.stock - y
					}
					if(update.stock <= 0){
						update = {
							stock: 0,
							isActive: false
						}
					}
					await Product.findByIdAndUpdate(x.id, update)
				}


			};
			await order.save()
			// This for loop will delete all cart of the user once order has been successfully process
			for(i = 0; i < userCarts.length; i++){
				await Cart.findByIdAndDelete(userCarts[i].id)
			};
			return "Cart has been successfully ordered" //true
		}
	}catch(error){
		return "Error" //false
	}
	
};
