// EXPRESS SERVER SETUP
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");
const cartRoutes = require("./routes/cartRoutes.js");

const app = express();
const port = process.env.PORT || 3000;


// MONGOOSE CONNECTION
mongoose.set("strictQuery", false)
mongoose.connect("mongodb+srv://Mambinunsad-224:Admin123@224-mambinunsad.uucqztz.mongodb.net/ecommerce-API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
// Use this to catch error instead of (db on/once)
.then(()=> console.log("Connected to MongoDB"))
.catch((error) => console.log("Connection Error"));


// let db = mongoose.connection;

// db.on("error", () => console.error("Connection error."));
// db.once("open", () => console.log("Connected to MongoDB."));


// MIDDLEWARES
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));



// MAIN URI
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/carts",cartRoutes);







// LISTEN
app.listen(port, () => {console.log(`API is now running at port: ${port}`)});