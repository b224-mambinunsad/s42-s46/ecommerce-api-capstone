Function that can do in this API

[SECTION] USER
1. User registration (https://e-commerce-api-04ez.onrender.com/users/register)
	-this function has the ability to check the email provided by user.
	-if it is not on the database it will return true if it is it will return false

2.Login Function (https://e-commerce-api-04ez.onrender.com/users/login)
	- this function will check both email and password is correct in the db
	- if it is it will return token if its not return false

3.Retrieve User Details (https://e-commerce-api-04ez.onrender.com/users/details)
	- this funtion will check the authenctication of the user
	- if it is it return true if its not false

4. Setting user as Admin (https://e-commerce-api-04ez.onrender.com/users/setAdmin)
	-this function will set user to be a admin.
	-this function is only accessible by Owner

5.Retrieving all details of Users (Owner Only) (https://e-commerce-api-04ez.onrender.com/users/getAllDetails)
	-this function allow the order to view all the detail of the users
	- is only accessible by Owner



[SECTION] PRODUCT

1. Add Product (https://e-commerce-api-04ez.onrender.com/products/addProduct)
	-this function has the ability to check the product name if it already on the db
	-if it is it will return product name is existing if it not is will save the product
	-accessible only for admin

2. Retrieve Products (https://e-commerce-api-04ez.onrender.com/products/)
	-this function allow all to retrieve all active products

3.Retrieving a single product (https://e-commerce-api-04ez.onrender.com/products/find/:productId)
	-this function allow all to retrieve a single product via id of product

4.Updating Product (https://e-commerce-api-04ez.onrender.com/products/update/:productId)
	-this function can update a single product via id of product
	-accessible only for admin

5.Archiving Product (https://e-commerce-api-04ez.onrender.com/products/archive/:productId)
	-this function can update the status of a product
	-accesible only for admin

6.Activating Product (https://e-commerce-api-04ez.onrender.com/products/activate/:productId)
	-this function can update the status of a product
	-accesible only for admin

7.Retrive all Products (https://e-commerce-api-04ez.onrender.com/products/allProducts)
	- this function has the ability to get all of the prodcuts
	-accessible only for admin



[SECTION] ORDER

1. Add Order (https://e-commerce-api-04ez.onrender.com/orders/addOrder/:productId)
	-this function allow authenticated user to order product
	-it has the ablity to automatically update the stock of the product based on the order quantity of the user
	- it also has the ability to automatically archived a product if the product stock became 0

2. Retrieving User Order (https://e-commerce-api-04ez.onrender.com/orders/myOrders)
	- this function can retrieve all order of the authenticated user
	- accessible only for user

3. Updating User Order (https://e-commerce-api-04ez.onrender.com/orders/myOrders/6392ecea05bc9911e9d3ae2e)
	-this function allow authenticated user to update address of his order via order id if the status of the order is pending.
	-it also has the ability to cancel user order if the status of the order is pending
	- this one also update the product stock/isActive if the user cancel his order

4. Getting All Order (https://e-commerce-api-04ez.onrender.com/orders/allOrders)
	-this function can get all authenticated user order based on the status of the order
	-accesible only for admin

5. Update Status of User Order (https://e-commerce-api-04ez.onrender.com/orders/updateOrder/:orderId)
	-this function can update the status of a single order via order id
	-accessible only for admin


[SECTION] CART (Authorize USER ONLY)

1. Add to Cart (https://e-commerce-api-04ez.onrender.com/carts/addToCart/:productId)
	- this function allows user to add product on there cart
	- it will also check if the product is already on users cart

2. Update Cart (https://e-commerce-api-04ez.onrender.com/carts/update/:cartId)
	- this function allow user to update the qty of the product in their cart

3. Delete Cart (https://e-commerce-api-04ez.onrender.com/carts/delete/:cartId)
	- this function allow user to delete the product on their cart

4. View Cart  (https://e-commerce-api-04ez.onrender.com/carts/)
	- this function will get all the products on user cart

5. Ordering All Product in the Cart (https://e-commerce-api-04ez.onrender.com/carts/orderCart)
	- this function allow user to order all products on their cart
	- it has the ability to update Product stock based on the id and qty that the user ordered
	- it also delete the cart of the user once the order has been successfully process